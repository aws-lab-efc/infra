provider "aws" {
  region = "us-east-1"
}

provider "gitlab" {}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.6.0"
    }

  }
  backend "s3" {
    region = "us-east-1"
    bucket = "terraform-testelab"
    key    = "terraform/lab/tfstate-gitlabci"
  }
}

resource "aws_security_group" "aws-lab-efc-sg" {
  name = "aws-lab-efc-sg"
  vpc_id      = data.aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "main" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  key_name        = "aws-gitlab-efc-key"
  subnet_id       = data.aws_subnet.main.id
  security_groups = [aws_security_group.aws-lab-efc-sg.id]
  tags = {
    Name = "aws-lab-efc-server"
  }
}

resource "gitlab_group_variable" "server_ip" {
  group = data.gitlab_group.main.group_id
  key   = "SERVER_IP"
  value = aws_instance.main.public_ip
}

output "server_ip" {
  value = aws_instance.main.public_ip
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_vpc" "main" {
  default =  true
}

data "aws_subnet" "main" {
  vpc_id = data.aws_vpc.main.id
}

data "gitlab_group" "main" {
  full_path = "aws-lab-efc"
}