# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.6.0"
  constraints = "3.6.0"
  hashes = [
    "h1:S0PE1Jk6gNtGetAJSj8ETPyRBA9a++647YAPeP47H0s=",
    "zh:08a915f21564737090eea015f47d7dd5c2d1a5f63b49211937534f643f47b8ca",
    "zh:145527bed51ec8070a85e967395fcd9aa32c97fbf2cf42747397cef6f1388213",
    "zh:1e3c8c26af5076b9067ac9f6f71b188c25838db42e8d6c11128322d6c4655982",
    "zh:5a795890e78121f7b8d8a3ae6fdc6a6f69083465b6a2073c9c0f0564dad4f7ec",
    "zh:765219c271e8af16fd436cf88e32079379b035d92bca0df556dee9adf9c9152f",
    "zh:7804d99ba61f74db1a4a911d181e7f8cee62c41658fe424cfa28362d6bbe8784",
    "zh:85e51fd6402de3e842e75ea02dc79cc3903fd030bd3d312bfcb78a8a61b8dfaa",
    "zh:8b13a75dcd932dc43616d058eeae60fada70bc93b0deb7152a4aba55d365e5b0",
    "zh:8d80014cc5acf9c4c64cd73e8f15b418b52cc54e112b1ebb49fdefa2ad8e13eb",
    "zh:b25129758f8aa6835dd22101f945abed6430d457659d20d2638eff27ca71bddd",
    "zh:b35e089343a6185db0196273517fcfe98fbd13d6ac1901b49d1d24b2b39b2ff1",
    "zh:cd928bc0e5d42fb966fd583cb8c46169cf80d35a57859db949d0c0b802fef35d",
    "zh:f8af5be58d41afcda522926e3768728fe412a87dbacca7ecb02341b854bf798f",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.70.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:6tf4jg37RrMHyVCql+fEgAFvX8JiqDognr+lk6rx7To=",
    "zh:01a5f351146434b418f9ff8d8cc956ddc801110f1cc8b139e01be2ff8c544605",
    "zh:1ec08abbaf09e3e0547511d48f77a1e2c89face2d55886b23f643011c76cb247",
    "zh:606d134fef7c1357c9d155aadbee6826bc22bc0115b6291d483bc1444291c3e1",
    "zh:67e31a71a5ecbbc96a1a6708c9cc300bbfe921c322320cdbb95b9002026387e1",
    "zh:75aa59ae6f0834ed7142c81569182a658e4c22724a34db5d10f7545857d8db0c",
    "zh:76880f29fca7a0a3ff1caef31d245af2fb12a40709d67262e099bc22d039a51d",
    "zh:aaeaf97ffc1f76714e68bc0242c7407484c783d604584c04ad0b267b6812b6dc",
    "zh:ae1f88d19cc85b2e9b6ef71994134d55ef7830fd02f1f3c58c0b3f2b90e8b337",
    "zh:b155bdda487461e7b3d6e3a8d5ce5c887a047e4d983512e81e2c8266009f2a1f",
    "zh:ba394a7c391a26c4a91da63ad680e83bde0bc1ecc0a0856e26e9d62a4e77c408",
    "zh:e243c9d91feb0979638f28eb26f89ebadc179c57a2bd299b5729fb52bd1902f2",
    "zh:f6c05e20d9a3fba76ca5f47206dde35e5b43b6821c6cbf57186164ce27ba9f15",
  ]
}
